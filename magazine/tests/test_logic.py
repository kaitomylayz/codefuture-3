from django.test import TestCase
from magazine.utils import *

class CalculateMoneyDefTestCase(TestCase, CalculateMoney):
    def test_sum_price_pass(self):
        list_price = [100,200,10000]
        result = self.sum_price(list_price)
        self.assertEqual(10300, result)

    def test_sum_price_count_pass(self):
        result = self.sum_price_count(price=400, count=19)
        self.assertEqual(7600, result)