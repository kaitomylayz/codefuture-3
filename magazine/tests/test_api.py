from rest_framework.test import APITestCase
from django.urls import reverse
from magazine.models import Product, Category
from magazine.serializers import ProductSerializer
from rest_framework import status

class ProductAPITestCase(APITestCase):
    def test_get_list(self):
        category_1 = Category.objects.create(name='Тестовая категория')
        product_1 = Product.objects.create(name='Test1', price=200, category=category_1)
        product_2 = Product.objects.create(name='Test2', price=190.65, category=category_1, description='Продукт для тестирования')
        url = '/magazine/api/products/'

        response = self.client.get(url)

        serializer_data = ProductSerializer([product_1, product_2], many=True).data

        self.assertEqual(status.HTTP_200_OK, response.status_code)
        self.assertEqual(serializer_data,response.data)

    def test_post_list(self):
        category_1 = Category.objects.create(name='Тестовая категория')
        product_1 = Product.objects.create(name='Test1', price=200, category=category_1)
        serializer_data = ProductSerializer(product_1).data
        url = '/magazine/api/products/'

        response = self.client.post(url, data={
           'name': 'Test1',
           'price': 200,
           'category': category_1.pk,
           'exists': True,
        })

        self.assertEqual(status.HTTP_201_CREATED, response.status_code)
        self.assertEqual(serializer_data.get('name'), response.data.get('name'))
        self.assertEqual(serializer_data.get('description'), response.data.get('description'))
        self.assertEqual(serializer_data.get('price'), response.data.get('price'))
        self.assertEqual(serializer_data.get('category'), response.data.get('category'))
        self.assertEqual(serializer_data.get('exists'), response.data.get('exists'))